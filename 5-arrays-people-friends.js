// 1. Make the balance into a number. If the number is invalid, default to 0. Do not overwrite the existing key, create a new one instead.
// 2. Sort by descending order of age.
// 3. Flatten the friends key into a basic array of names.
// 4. Remove the tags key.
// 5. Make the registered date into the DD/MM/YYYY format.
// 6. Filter only the active users.
// 7. Calculate the total balance available.

// Chain everything.


let data = require("./dataset")

function validBalance(data) {
    return data.map((person) => {
        let balance = Number(person["balance"].replace(",", "").replace("$", ""));
        person["validBalance"] = !isNaN(balance) ? balance : 0
        return person;
    })

}


function sortAge(data) {
    return validBalance(data).sort((first, second) => {
        return second.age - first.age;
    })
}


function flattenFriends(data) {
    return sortAge(data).map((person) => {
        person["flattenFriend"] = person["friends"].map((friend) => {
            return friend["name"];
        })
        return person;
    })
}


function removeTags(data) {
    return flattenFriends(data).map((person) => {
        delete person["tags"];
        return person;
    })
}


function dateFormatChange(data) {
    return removeTags(data).map((person) => {
        let date = person["registered"].split("T")[0].split("-").reverse().join("/");
        person["newFormatRegisteredData"] = date;
        return person;
    })
}

function filterActiveUsers(data) {
    return dateFormatChange(data).filter((person) => {
        return person.isActive;
    })
}   


function calculateTotalBalance(data) {
    return filterActiveUsers(data).reduce((acc, curr) => {
        return acc + curr.validBalance;
    }, 0)
}

